import Formulario from './components/Formulario';
import 'bootstrap/dist/css/bootstrap.css'
import React from 'react';

function App() {

  const bkImg = {
    'backgroundImage': 'url("https://forum-content.supercell.com/images/201401/coc_background_cropped.jpg")',
    'backgroundSize': 'cover',
    'height': '12em'
  }
  const headerImg = React.createElement('header', {style: bkImg})

  return (
    <>
      {headerImg}
      <div className="container">
        <h1 className='mt-4 mb-3 mx-auto text-center font-weight-bold' data-testid='encabezadoPrincipal'>Clash of clans</h1>
        <p>Clash of Clans, también conocido como CoC, es un videojuego de estrategia y de construcción de aldeas en línea, para dispositivos móviles con plataformas de IOS y Android.</p>
        <Formulario />
      </div>
    </>
  );
}

export default App;
