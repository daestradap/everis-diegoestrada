import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import App from './App';
import Formulario from '../src/components/Formulario'

test('<App /> Carga inicial index', () => {
  render(<App />);
  const headingOneElement = screen.getByTestId('encabezadoPrincipal');
  expect(headingOneElement).toBeInTheDocument();
  expect(headingOneElement.tagName).toBe('H1');
  expect(headingOneElement.tagName).not.toBe('H2');
  expect(headingOneElement.textContent).toBe('Clash of clans');
});

test('<Formulario /> Carga inicial del formulario', () => {
  render(<Formulario />);
  expect(screen.getByText(/Buscador de Clanes/i)).toBeInTheDocument()
  const txtBuscar = screen.getByTestId('txtName');
  expect(txtBuscar.tagName).toBe('INPUT');
  const txtMin = screen.getByTestId('txtMinMembers');
  expect(txtMin.tagName).toBe('INPUT');
  const txtMax = screen.getByTestId('txtMaxMembers');
  expect(txtMax.tagName).toBe('INPUT');
  const btnSubmit = screen.getByTestId('btnSubmit');
  expect(btnSubmit.tagName).toBe('BUTTON');
  expect(btnSubmit.tagName).not.toBe('INPUT');
  expect(btnSubmit.textContent).toBe('Buscar');
});

test('<Formulario /> Error diligenciar formulario', () => {
  render(<Formulario />);
  const txtBuscar = screen.getByTestId('txtName');
  expect(txtBuscar.tagName).toBe('INPUT');
  const txtMin = screen.getByTestId('txtMinMembers');
  expect(txtMin.tagName).toBe('INPUT');
  const txtMax = screen.getByTestId('txtMaxMembers');
  expect(txtMax.tagName).toBe('INPUT');
  const btnSubmit = screen.getByTestId('btnSubmit');
  expect(btnSubmit.tagName).toBe('BUTTON');
  expect(btnSubmit.tagName).not.toBe('INPUT');
  userEvent.click(btnSubmit)
  //check error
  const alerta = screen.getByTestId('alerta');
  expect(alerta).toBeInTheDocument();
  expect(alerta.tagName).toBe('DIV');
  expect(alerta.textContent).toBe('Ingresar todos de los campos')
});
