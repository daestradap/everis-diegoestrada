
const Alerta = ({mensaje}) => {
    return (
        <div 
            className='alert alert-danger' 
            role='alert' 
            id='msgalerta'
            data-testid='alerta'
        >
            {mensaje}
        </div>
    );
}
 
export default Alerta;
