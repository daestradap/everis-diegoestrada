
const Card = ({datos}) => {

    const show = () => {
        if(Object.entries(datos).length !== 0) {
            datos.map(element => 
                (
                    <div className="card col-md-4" key={element.tag} style={{'float': 'left', 'height': '500px', 'overflow-y': 'scroll'}}>
                        <img className="card-img-top" src={element.badgeUrls.medium} alt={element.name} />
                        <div className="card-body" style={{'text-align': 'center', 'padding': '1rem 0'}}>
                            <h5 className="card-title mx-auto"><b>{element.name}</b> Lvl#{element.clanLevel}</h5>
                            <p className="card-text mx-auto"><b>Tag: </b>{element.tag}</p>
                            <p className="card-text mx-auto">Type: {element.type}</p>
                            <p className="card-text mx-auto">Members: {element.members} | War frequency: {element.warFrequency}</p>
                        </div>
                    </div>
                )
            );
        } else {
            return null;
        }
    }

    return show();
}
 
export default Card;
