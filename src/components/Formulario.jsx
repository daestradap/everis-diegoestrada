import { useState } from 'react';
import axios from 'axios';
// import items from '../data/api.clashofclans.com.json'
import Cards from './Card';
import Alerta from './Alerta';

const Formulario = () => {

    //state de formulario
    const [datos, setDatos] = useState({
        name: '',
        minMembers: '',
        maxMembers: '',
        Authorization: ''
    });
    const {name, minMembers, maxMembers} = datos;
    const [card, setCard] = useState(false);
    const [dataApi, setDataApi] = useState({});
    const [alerta, setAlerta] = useState(null);
    const [msjalerta, setmsjAlerta] = useState('');

    const actualizarState = e => {
        setDatos({
            ...datos,
            [e.target.name] : e.target.value
        })
        return;
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if(name.trim() === '' && minMembers.trim() === '' && maxMembers.trim() === '') {
            setAlerta(true)
            setmsjAlerta('Ingresar todos de los campos');
            setDataApi({});
            setCard(false);
            return;
        }
        consultarAPICoc();
    }

    const consultarAPICoc = async () => {
        try {
            const token='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6ImY2OWE0MzlmLTE4NTMtNDMxZS1hMjgxLTAzNTY2NTNhYjBlYSIsImlhdCI6MTYyNDQ2MDQ4NSwic3ViIjoiZGV2ZWxvcGVyL2YwOGViNWZmLWUwMDItNzQ0Mi05Mzg5LWE1YjNmYjIyNmU0NyIsInNjb3BlcyI6WyJjbGFzaCJdLCJsaW1pdHMiOlt7InRpZXIiOiJkZXZlbG9wZXIvc2lsdmVyIiwidHlwZSI6InRocm90dGxpbmcifSx7ImNpZHJzIjpbIjE4Ni4zMC45MS41MSJdLCJ0eXBlIjoiY2xpZW50In1dfQ.sGyKTnYp6Em5BA0oCvj5AuWqiFWp_LhuK2gsdVQ-QW3NPEf4MsjSv5IO3YdbQInag_5xs2LVZozKZN2K1ZW_dA'
            datos.Authorization = `Bearer ${token}`
            const clienteAxios = axios.create({
                baseURL: 'https://api.clashofclans.com/v1/',
                Accept: 'application/json',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Access-Control-Allow-Origin': '*'
    }
            });
            const resultado = await clienteAxios.get('/clans', { params:  datos });
            console.log(resultado);
            setDataApi(resultado);
            setCard(true);
            setAlerta(false);
            setmsjAlerta('');
            /* let rts = items.items.filter(elm => 
               elm.name.toLocaleLowerCase().includes(name.toLocaleLowerCase()) && 
               Number.parseInt(elm.members) >= Number.parseInt(minMembers) && 
               Number.parseInt(elm.members) <= Number.parseInt(maxMembers)
            ); */
        } catch (error) {
            console.log(error.response);
            setDataApi({})
            setCard(false);
            setAlerta(true);
            setmsjAlerta('No hay resultados que mostrar, puede intentar otra busqueda');
        }
        return;
    }

    return (
        <>
            <form
                className='form-group'
                onSubmit={ handleSubmit }
                data-testid='form'
            >
                <div className='col-md-12 mb-2 mt-2'>
                    <label htmlFor='name'>Buscador de Clanes</label>
                    <input
                        type='text'
                        placeholder='Ingresar el nombre de un clan'
                        name='name'
                        className='form-control'
                        onChange={actualizarState}
                        data-testid='txtName'
                    ></input>
                </div>
                
                <div className='col-md-6 float-left mb-2 mt-2'>
                    <label htmlFor='minMembers'>Número mínimo de participantes</label>
                    <input
                        type='text'
                        placeholder='Número mínimo de participantes'
                        name='minMembers'
                        className='form-control'
                        onChange={actualizarState}
                        data-testid='txtMinMembers'
                    ></input>
                </div>
                
                <div className='col-md-6 float-right mb-2 mt-2'>
                    <label htmlFor='maxMembers'>Número máximo de participantes</label>
                    <input
                        type='text'
                        placeholder='Número máximo de participantes del clan'
                        name='maxMembers'
                        className='form-control'
                        onChange={actualizarState}
                        data-testid='txtMaxMembers'
                    ></input>
                </div>
                
                <button
                    type='submit' 
                    className='btn btn-primary mb-2 mt-2'
                    data-testid='btnSubmit'
                >Buscar</button>
            </form>

            {
                card ? <Cards datos={dataApi} /> : null
            }

            {
                alerta ? <Alerta mensaje={msjalerta} /> : null
            }
        </>
    );
}

export default Formulario;
